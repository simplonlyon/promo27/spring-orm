# Sring ORM
Projet Spring Boot avec utilisation d'un ORM, authentification et upload d'image

## Dépendances utilisées
* Spring Security pour l'authentification
* Spring Data JPA pour l'ORM
* Thumbnailator pour la création de miniature

## ORM
Un ORM permet de gérer la couche base de données d'une application, grâce à lui, on peut se passer de faire du SQL (même si cela reste possible et configurable) et les repositories sont également pris en charge.

### Annotations de l'entité
Les seules annotations obligatoires sont le `@Entity` sur l'entité et le `@Id` pour identifier la clé primaire de celle ci. On utilise ici également une `@GeneratedValue` pour indiquer que la valeur de la clé primaire est générée automatiquement.

Si besoin de personnalisés certaines propriétés, on peut utiliser l'annotation `@Column` dans laquelle on peut indiquer le type de colonne à généré dans la base de données, si elle est nullable ou autre.

```java
@Entity
public class Exemple {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) 
    private Integer id;
    private String uneProp;
    @Column(columnDefinition = "TEXT")
    private String unLongTexte;

    //Getters & setters
}
```

### Annotation pour les relations
On peut marquer certaines propriétés comme étant des relation, OneToMany, ManyToOne, ManyToMany, OneToOne. Les relations peuvent être bidirectionnelles (accessible depuis chaque entités) ou unidirectionnelle (accessible seulement depuis l'une des deux entités). Dans le cas d'une relation bidirectionnelle, il faudra indiqué un mappedBy sur un des côtés pour lier les deux relations entre elles.

```java
@Entity
public class Address {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) 
    private Integer id;
    // ...
    @ManyToOne
    private Person owner;

}
```
```java
@Entity
public class Person {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) 
    private Integer id;
    // ...
    @OneToMany(mappedBy = "owner") //On indique du côté OneToMany à quelle propriété de Address est liée cette relation
    private List<Address> addresses = new ArrayList<>();

}
```

### Repository
On peut définir un repository avec une interface qui extends JpaRepository en indiquant le nom de la classe et le type de sa clé primaire. Un CRUD (avec pagination possible) est automatiquement généré.

Il est possible de rajouter des nouvelles requêtes dans le repository qui peuvent se baser automatiquement sur le nom de la méthode en suivant [ces spécifications](https://docs.spring.io/spring-data/jpa/reference/jpa/query-methods.html#jpa.query-methods.query-creation)

On peut aussi faire des méthodes avec des requêtes custom dans certains cas plus complexes. On utilisera l'annotation `@Query`.


```java
@Repository
public interface PersonRepository extends JpaRepository<Person, Integer> {

    List<Person> findByName(String name); //Génère une méthode avec un WHERE name=?
    
    @Query("SELECT p FROM Person p WHERE p.name LIKE %?1% OR p.firstName LIKE %?1%")
    List<Person> search(String term);
}
```


## Upload de fichiers
L'upload de fichier consiste à créer une route dans un contrôleur qui attendra une requête en multipart/form-data (alors qu'habituellement les requêtes contiennent plutôt du application/json) qui contiendra un fichier envoyé par le client.

Une fois le fichier reçu, on fait en sorte de modifier son nom pour que celui ci soit unique (comme ça si 2 personnes upload un fichier qui s'appelle image.jpg, l'une n'ecrasera pas l'autre), éventuellement créer une miniature (thumbnail) puis stocker le fichier dans un dossier sur le serveur qui est accessible dans un serveur HTTP.

### La Route upload
On définit une route d'upload en post qui attendra un MultiPartFile en @RequestParam, dedans on crée un nom de fichier unique avec UUID (ça serait possible avec d'autres méthodes) puis on utilise une library Thumbnailator qui permet de redimensionner et crop des images facilement (ça serait possible avec d'autres library ou avec du pur Java). Ici on crée une miniature de 200x200 avec un crop et on sauvegarde sur le serveur une version de l'image uploadé de 900px de width (histoire de ne pas sauvegarder des images trop volumineuses sur le serveur. Le choix de 900 est complétement arbitraire et pourrait être une variable du application.properties)
```java
@PostMapping("/upload")
    public String upload(@RequestParam MultipartFile image) {
        String renamed = UUID.randomUUID()+".jpg";
        try {
            Thumbnails.of(image.getInputStream())
            .width(900)
            .toFile(new File(getUploadFolder(), renamed));
            Thumbnails.of(image.getInputStream())
            .size(200,200)
            .crop(Positions.CENTER)
            .toFile(new File(getUploadFolder(), "thumbnail-"+renamed));
        } catch (IOException e) {
           throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Upload error", e);
        }
        return renamed;
    }
```

### Chemin vers le dossier d'uploads
Ici on crée une méthode qui récupère le chemin vers le dossier dans lequel les images seront uploadées. Dans ce projet, on a choisi de les mettre dans le dossier resources/static/uploads. Le dossier resources/static est automatiquement rendu disponible par le Spring Boot sur localhost:8080.

```java
private File getUploadFolder() {
    File folder =
        new File(getClass().getClassLoader().getResource(".").getPath().concat("static/uploads"));
    if (!folder.exists()) {
        folder.mkdirs();
    }
    return folder;

}
```

### Getters dans l'entité
On peut rajouter des getters dans l'entité qui permettront d'accéder aux liens complets du serveur avec le chemin vers le dossier d'uploads.

Ici, dans le cas d'une image `mon-image.jpg`, ces getters rendront disponible une propriété imageLink au front qui contiendra `http://localhost:8080/uploads/mon-image.jpg` et une propriété thumbnailLink qui contiendra `http://localhost:8080/uploads/thumbnail-mon-image.jpg`
```java
    public String getImageLink() {
        if(image.startsWith("http")) {
            return image;
        }
        final String baseUrl = 
ServletUriComponentsBuilder.fromCurrentContextPath().build().toUriString();
        return baseUrl+"/uploads/"+image;
    }

    public String getThumbnailLink() {
        if(image.startsWith("http")) {
            return image;
        }
        final String baseUrl = 
ServletUriComponentsBuilder.fromCurrentContextPath().build().toUriString();
        return baseUrl+"/uploads/thumbnail-"+image;
    }
```

### Côté front
Il faudra utiliser un `<input type="file" >` (ou équivalent avec une library d'UI) et lier celui ci à une `ref<File[]>()` dans le cas où on utilise vue.

Il faudra ensuite faire une requête axios spéciale avec 
```js
axios.postForm('/lien/vers/upload', {
    image: laFile
})
```