package co.simplon.promo27.springorm;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.security.test.context.support.WithUserDetails;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@SpringBootTest
@AutoConfigureMockMvc
class SpringormApplicationTests {

	@Autowired
	MockMvc mvc;
	
	@Test
	@WithUserDetails("user@test.com")
	void testGetMyPictures() throws Exception {
		mvc.perform(get("/api/account/picture"))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$[*]['description']").exists());
	}

}
