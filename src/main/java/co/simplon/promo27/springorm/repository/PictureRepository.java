package co.simplon.promo27.springorm.repository;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import co.simplon.promo27.springorm.entity.Picture;

@Repository
public interface PictureRepository extends JpaRepository<Picture, Integer> {

    @Query("select p from Picture p where p.title like %?1% or p.description like %?1% or p.author.email like %?1%")
    Page<Picture> search(String term, Pageable pageable);

    @Query("select p from Picture p where p.author.id=?1")
    Page<Picture> findByUserId(int idUser, Pageable pageable);
}
