package co.simplon.promo27.springorm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.simplon.promo27.springorm.entity.Comment;
@Repository
public interface CommentRepository extends JpaRepository<Comment, Integer> {

}
