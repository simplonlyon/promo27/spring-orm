package co.simplon.promo27.springorm.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.promo27.springorm.entity.Picture;
import co.simplon.promo27.springorm.entity.User;
import co.simplon.promo27.springorm.repository.PictureRepository;
import co.simplon.promo27.springorm.repository.UserRepository;
import jakarta.validation.Valid;

@RestController
public class AuthController {

    @Autowired
    private UserRepository userRepo;
    @Autowired
    private PictureRepository picRepo;
    @Autowired
    private PasswordEncoder encoder;

    @GetMapping("/api/account")
    public User account(@AuthenticationPrincipal User user) {
        return user;
    }

    @GetMapping("/api/user/available/{email}")
    public boolean availableEmail(@PathVariable String email) {
        if(userRepo.findByEmail(email).isPresent()) {
            return false;
        }
        return true;
    }

    @PostMapping("/api/user")
    @ResponseStatus(HttpStatus.CREATED)
    public User register(@Valid @RequestBody User user) {
        if(userRepo.findByEmail(user.getEmail()).isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "User already exists");
        }
        user.setRole("ROLE_USER");
        user.setPassword(encoder.encode(user.getPassword()));
        userRepo.save(user);
        return user;
    }

    
    @GetMapping("/api/account/picture")
    public List<Picture> myPictures(@AuthenticationPrincipal User user) {
        Picture picture = new Picture();
        picture.setAuthor(user);
        return picRepo.findAll(Example.of(picture));
        //Le User est un cas particulier qui est détaché de JPA, on ne peut donc pas charger
        //directement ses relations. On peut donc faire une requête comme au dessus
        //Ou bien avec un EntityManager autowired et un @Transactional sur la méthode :
        /*
         * user = em.merge(user);
         * return user.getPictures();
         */
    }


    @DeleteMapping("/api/account")
    public void deleteAccount(@AuthenticationPrincipal User user) {
        userRepo.delete(user);
    }
}
