package co.simplon.promo27.springorm.controller;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.promo27.springorm.entity.Comment;
import co.simplon.promo27.springorm.entity.Picture;
import co.simplon.promo27.springorm.entity.User;
import co.simplon.promo27.springorm.repository.PictureRepository;
import co.simplon.promo27.springorm.repository.UserRepository;
import jakarta.validation.Valid;
import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.geometry.Positions;

@RestController
@RequestMapping("/api/picture")
public class PictureController {

    @Autowired
    private PictureRepository repo;
    @Autowired
    private UserRepository userRepo;

    @GetMapping
    public Page<Picture> all(@RequestParam(required = false) String search, 
                            @RequestParam(defaultValue = "0") int pageNumber, 
                            @RequestParam(defaultValue = "5") int pageSize) {
        if(pageSize > 30){
            pageSize = 30;
        }
        Pageable page = Pageable.ofSize(pageSize).withPage(pageNumber);
        if(search != null) {
            return repo.search(search,page);
        }
        return repo.findAll(page);
    }

    @GetMapping("/{id}")
    public Picture one(@PathVariable int id) {
        return repo.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Picture add(@Valid @RequestBody Picture picture, @AuthenticationPrincipal User user) {
        picture.setCreatedAt(LocalDateTime.now());
        picture.setAuthor(user);
        repo.save(picture);
        return picture;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable int id, @AuthenticationPrincipal User user) {
        Picture pic = one(id);
        if (!pic.getAuthor().getId().equals(user.getId())) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Not your picture");
        }
        repo.delete(pic);
    }

    @PutMapping("/{id}")
    public Picture update(@PathVariable int id, @Valid @RequestBody Picture picture,
            @AuthenticationPrincipal User user) {
        Picture toUpdate = one(id);
        if (!toUpdate.getAuthor().getId().equals(user.getId())) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Not your picture");
        }
        toUpdate.setImage(picture.getImage());
        toUpdate.setDescription(picture.getDescription());
        toUpdate.setTitle(picture.getTitle());
        repo.save(toUpdate);
        return toUpdate;

    }

    @PostMapping("/upload")
    public String upload(@RequestParam MultipartFile image) {
        String renamed = UUID.randomUUID() + ".jpg";
        try {
            Thumbnails.of(image.getInputStream())
                    .width(900)
                    .toFile(new File(getUploadFolder(), renamed));
            Thumbnails.of(image.getInputStream())
                    .size(200, 200)
                    .crop(Positions.CENTER)
                    .toFile(new File(getUploadFolder(), "thumbnail-" + renamed));
        } catch (IOException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Upload error", e);
        }
        return renamed;
    }

    /**
     * Méthode qui récupère le lien vers le dossier d'uploads et le crée s'il
     * n'existe pas.
     * Il est ici configuré pour le mettre dans le dossier resources/static qui
     * existe et est
     * servi automatiquement par Spring Boot
     * 
     * @return
     */
    private File getUploadFolder() {
        File folder = new File(getClass().getClassLoader().getResource(".").getPath().concat("static/uploads"));
        if (!folder.exists()) {
            folder.mkdirs();
        }
        return folder;

    }

    @GetMapping("/{id}/comment")
    public List<Comment> getComments(@PathVariable int id) {
        Picture picture = one(id);
        return picture.getComments();
    }

    @PatchMapping("/{id}/like")
    public Picture like(@PathVariable int id, @AuthenticationPrincipal User user) {
        Picture picture = one(id);
        
        if(!picture.getLikes().removeIf(item -> item.getId().equals(user.getId()))) {
            picture.getLikes().add(user);
        }
        repo.save(picture);
        return picture;
    }

    @GetMapping("/user/{idUser}")
    public Page<Picture> getUserPictures(@PathVariable int idUser,
                                        @RequestParam(defaultValue = "0") int pageNumber, 
                                        @RequestParam(defaultValue = "5") int pageSize) {
        if(pageSize > 30){
            pageSize = 30;
        }
        Pageable page = Pageable.ofSize(pageSize).withPage(pageNumber);
        return repo.findByUserId(idUser, page);
        //Même chose avec un findAll par Example qui ne nécessite pas de faire une requête custom dans
        //le repository, un poil moins optimisée et réutilisable que la manière au dessus
        // User user = userRepo.findById(idUser)
        // .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
        // Picture picture = new Picture();
        // picture.setAuthor(user);
        // if(pageSize > 30){
        //     pageSize = 30;
        // }
        // Pageable page = Pageable.ofSize(pageSize).withPage(pageNumber);
        // return repo.findAll(Example.of(picture), page);

    }
}
