package co.simplon.promo27.springorm.controller;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import co.simplon.promo27.springorm.entity.Comment;
import co.simplon.promo27.springorm.entity.User;
import co.simplon.promo27.springorm.repository.CommentRepository;
import jakarta.validation.Valid;

@RequestMapping("/api/comment")
@RestController
public class CommentController {

    @Autowired
    private CommentRepository repo;
    
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Comment add(@RequestBody @Valid Comment comment, @AuthenticationPrincipal User user) {
        comment.setAuthor(user);
        comment.setCreatedAt(LocalDateTime.now());
        repo.save(comment);
        return comment;
    }
}
