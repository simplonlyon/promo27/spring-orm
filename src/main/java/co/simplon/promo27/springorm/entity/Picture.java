package co.simplon.promo27.springorm.entity;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fasterxml.jackson.annotation.JsonIgnore;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.validation.constraints.NotBlank;

@Entity
public class Picture {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) 
    private Integer id;
    @NotBlank
    private String image;
    @NotBlank
    @Column(columnDefinition = "TEXT")
    private String description;
    @NotBlank
    private String title;
    private LocalDateTime createdAt;
    @JsonIgnore
    @OneToMany(mappedBy = "picture")
    private List<Comment> comments = new ArrayList<>();
    @ManyToOne(fetch = FetchType.EAGER) //Pour récupérer les user avec un left join
    private User author;
    @ManyToMany
    private List<User> likes = new ArrayList<>();

    public List<User> getLikes() {
        return likes;
    }
    public void setLikes(List<User> likes) {
        this.likes = likes;
    }
    public User getAuthor() {
        return author;
    }
    public void setAuthor(User author) {
        this.author = author;
    }
    public List<Comment> getComments() {
        return comments;
    }
    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getImage() {
        return image;
    }
    public void setImage(String image) {
        this.image = image;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public LocalDateTime getCreatedAt() {
        return createdAt;
    }
    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public String getImageLink() {
        if(image.startsWith("http")) {
            return image;
        }
        final String baseUrl = 
ServletUriComponentsBuilder.fromCurrentContextPath().build().toUriString();
        return baseUrl+"/uploads/"+image;
    }

    public String getThumbnailLink() {
        if(image.startsWith("http")) {
            return image;
        }
        final String baseUrl = 
ServletUriComponentsBuilder.fromCurrentContextPath().build().toUriString();
        return baseUrl+"/uploads/thumbnail-"+image;
    }
    
}
