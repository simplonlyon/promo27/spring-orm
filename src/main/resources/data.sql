INSERT INTO user (display_name,email,password,role) VALUES 
('admin','admin@test.com', '$2y$10$YRrX4d8PFtNJSWqdG6r73eQxE8ujmMZ49h.2.3cHN9I9K62.KDhPK', 'ROLE_ADMIN'),
('user','user@test.com', '$2y$10$YRrX4d8PFtNJSWqdG6r73eQxE8ujmMZ49h.2.3cHN9I9K62.KDhPK', 'ROLE_USER'),
('rando','rando@calrissian.com', '$2y$10$YRrX4d8PFtNJSWqdG6r73eQxE8ujmMZ49h.2.3cHN9I9K62.KDhPK', 'ROLE_USER');

INSERT INTO picture (title,description,image,created_at,author_id) VALUES 
('Test Image 1', 'A test description', 'https://picsum.photos/seed/picture1/200', '2024-04-23T17:13:18', 1),
('Test Image 2', 'A test description', 'https://picsum.photos/seed/picture2/200', '2024-04-23T17:13:18', 1),
('Test Image 3', 'A test description', 'https://picsum.photos/seed/picture3/200', '2024-04-23T17:13:18', 1),
('Test Image 4', 'A test description', 'https://picsum.photos/seed/picture4/200', '2024-04-23T17:13:18', 3),
('Test Image 5', 'A test description', 'https://picsum.photos/seed/picture5/200', '2024-04-23T17:13:18', 3),
('Test Image 6', 'A test description', 'https://picsum.photos/seed/picture6/200', '2024-04-23T17:13:18', 2),
('Test Image 7', 'A test description', 'https://picsum.photos/seed/picture7/200', '2024-04-23T17:13:18', 2),
('Test Image 8', 'A test description', 'https://picsum.photos/seed/picture8/200', '2024-04-23T17:13:18', 2),
('Test Image 9', 'A test description', 'https://picsum.photos/seed/picture9/200', '2024-04-23T17:13:18', 1),
('Test Image 10', 'A test description', 'https://picsum.photos/seed/picture10/200', '2024-04-23T17:13:18', 1),
('Test Image 11', 'A test description', 'https://picsum.photos/seed/picture11/200', '2024-04-23T17:13:18', 2),
('Test Image 12', 'A test description', 'https://picsum.photos/seed/picture12/200', '2024-04-23T17:13:18', 3),
('Test Image 13', 'A test description', 'https://picsum.photos/seed/picture13/200', '2024-04-23T17:13:18', 3),
('Test Image 14', 'A test description', 'https://picsum.photos/seed/picture14/200', '2024-04-23T17:13:18', 3),
('Test Image 15', 'A test description', 'https://picsum.photos/seed/picture15/200', '2024-04-23T17:13:18', 3);


INSERT INTO comment (content,created_at,picture_id,author_id) VALUES 
('A nice commentary', '2024-04-23T17:13:18', 1, 3),
('Another commentary', '2024-04-23T17:13:18', 1, 2);